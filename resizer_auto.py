#!/usr/bin/python
# -*- coding: utf-8 -*-
# Fork of https://github.com/theopolisme/theobot/tree/master/NonFreeImageResizer
# User:Wcam
from PIL import Image
from xml.dom import minidom
import cStringIO
import mwclient
import uuid
import urllib
import os.path
import cgi
import littleimage
import sys
import urllib2
import re
import logging
import time
import random
import pandas as pd


# CC-BY-SA Theopolisme
# Task 1 on [[User:Theo's Little Bot]]

logger = logging.getLogger('resizer_auto')
hdlr = logging.FileHandler('resizer_auto.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr) 
logger.setLevel(logging.WARNING)

def sokay(donenow):
	"""This function calls a subfunction
	of the theobot module, checkpage().
	"""
	# if donenow % 5 == 0:
	# 	if theobot.bot.checkpage("User:Theo's Little Bot/disable/resizer") == True:
	# 		return True
	# 	else:
	# 		return False
	# else:
	# 	return True
	return True

def are_you_still_there(theimage):
	""" This function makes sure that
	a given image is still tagged with
	{{non-free reduce}}.
	"""
	img_name = "File:" + theimage
		
	page = site.Pages[img_name]
	text = page.text()
	
	# r1 = re.compile(r'\{\{[Nn]on.?free-?\s*[Rr]educe.*?\}\}')
	# r2 = re.compile(r'\{\{[Rr]educe.*?\}\}')
	# r3 = re.compile(r'\{\{[Cc]omic-ovrsize-img.*?\}\}')
	# r4 = re.compile(r'\{\{[Ff]air.?[Uu]se.?[Rr]educe.*?\}\}')
	# r5 = re.compile(r'\{\{[Ii]mage-toobig.*?\}\}')
	# r6 = re.compile(r'\{\{[Nn]fr.*?\}\}')
	# r7 = re.compile(r'\{\{[Ss]maller image.*?\}\}')
	
	# if r1.search(text) is not None:
	# 	return True
	# elif r2.search(text) is not None:
	# 	return True
	# elif r3.search(text) is not None:
	# 	return True
	# elif r4.search(text) is not None:
	# 	return True
	# elif r5.search(text) is not None:
	# 	return True
	# elif r6.search(text) is not None:
	# 	return True
	# elif r7.search(text) is not None:
	# 	return True
	# else:
	# 	return False
	if not page.exists:
		print 'File not found! Skipping...'
		return False
	elif (u'Category:應轉移至維基共享資源的檔案' in [i.name for i in page.categories()]):
		return False
	else:
		return True

	# return True

def image_routine(images):
	""" This function does most of the work:
	* First, checks the checkpage using sokay()
	* Then makes sure the image file still exists using are_you_still_there()
	* Next it actually resizes the image.
	* As long as the resize works, we reupload the file.
	* Then we update the page with {{non-free reduced}}.
	* And repeat!
	"""
	donenow = 5
	for theimage in images:
		
		#print "Working on " + theimage.encode('ascii', 'ignore')
		print "Working on " + theimage.encode("utf-8")
		if sokay(donenow) == True:
			if are_you_still_there(theimage) == True:	
				desired_megapixel = float(0.1)
				pxl = desired_megapixel * 1000000
				compound_site = 'zh.wikipedia.org'
				#compound_site = 'test2.wikipedia.org'
				filename = str(uuid.uuid4())
				file = littleimage.gimme_image(filename,compound_site,pxl,theimage)
				
				if file == "SKIP":
					print "Skipping GIF."
					messager12345 = "Skipped gif: " + theimage
					logger.error(messager12345)
				
				if file == "PIXEL":
					print "Removing tag...already reduced..."
					img_name = "File:" + theimage
					page = site.Pages[img_name]
					text = page.text()
					text = re.sub(r'\{\{[Nn]on.?free-?\s*[Rr]educe.*?\}\}', '', text)
					text = re.sub(r'\{\{[Rr]educe.*?\}\}', '', text)
					text = re.sub(r'\{\{[Cc]omic-ovrsize-img.*?\}\}', '', text)			
					text = re.sub(r'\{\{[Ff]air.?[Uu]se.?[Rr]educe.*?\}\}', '', text)			
					text = re.sub(r'\{\{[Ii]mage-toobig.*?\}\}', '', text)			
					text = re.sub(r'\{\{[Nn]fr.*?\}\}', '', text)			
					text = re.sub(r'\{\{[Ss]maller image.*?\}\}', '', text)			
					page.save(text, summary = '机器人：图片已缩小，移除 {{[[Template:Non-free reduce|Non-free reduce]]}} ')				
					
				elif file not in ("ERROR", "PIXEL", "SKIP"):					
					try:
						site.upload(open(file), theimage, u'机器人：根据[[WP:NFCC]]#3b缩小非自由图片大小', ignore=True)
						
						print "Uploaded!"
						filelist = [ f for f in os.listdir(".") if f.startswith(filename) ]
						for fa in filelist: os.remove(fa)
						img_name = "File:" + theimage
						
						page = site.Pages[img_name]
						text = page.text()
						# text = re.sub(r'\{\{[Nn]on.?free-?\s*[Rr]educe.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)
						# text = re.sub(r'\{\{[Rr]educe.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)
						# text = re.sub(r'\{\{[Cc]omic-ovrsize-img.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)			
						# text = re.sub(r'\{\{[Ff]air.?[Uu]se.?[Rr]educe.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)			
						# text = re.sub(r'\{\{[Ii]mage-toobig.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)			
						# text = re.sub(r'\{\{[Nn]fr.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)			
						# text = re.sub(r'\{\{[Ss]maller image.*?\}\}', '{{non-free reduced|date=~~~~~}}', text)									
						# page.save(text, summary = "Tagging with {{[[Template:Non-free reduced|Non-free reduced]]}}")
						text = re.sub(r'\{\{[Nn]on.?free-?\s*[Rr]educe.*?\}\}', '', text)
						text = re.sub(r'\{\{[Rr]educe.*?\}\}', '', text)
						text = re.sub(r'\{\{[Cc]omic-ovrsize-img.*?\}\}', '', text)			
						text = re.sub(r'\{\{[Ff]air.?[Uu]se.?[Rr]educe.*?\}\}', '', text)			
						text = re.sub(r'\{\{[Ii]mage-toobig.*?\}\}', '', text)			
						text = re.sub(r'\{\{[Nn]fr.*?\}\}', '', text)			
						text = re.sub(r'\{\{[Ss]maller image.*?\}\}', '', text)			
						page.save(text, summary = "机器人：图片已缩小，移除 {{[[Template:Non-free reduce|Non-free reduce]]}}", watch = False , unwatch = False)				
						
						print "Tag removed!"
					except:
						print "Unknown error. Image skipped."
						messager12345 = "Unknown error; skipped " + theimage
						logger.error(messager12345)
						filelist = [ f for f in os.listdir(".") if f.startswith(filename) ]
						for fa in filelist: os.remove(fa)
						
				else:
					print "Image skipped."
					messager123 = "Skipped " + theimage
					filelist = [ f for f in os.listdir(".") if f.startswith(filename) ]
					for fa in filelist: os.remove(fa)
					logger.error(messager123)
			else:
				print "Gah, looks like someone removed the tag."
				messager1234 = "Tag removed on image; skipped " + theimage
				logger.error(messager1234)

		else:
			print "Ah, darn - looks like the bot was disabled."
			sys.exit()
		donenow = donenow+1
		time.sleep(5)
		# time.sleep(random.randint(300, 600))

def main():
	"""This defines and fills a global
	variable for the site, and then calls
	get_images() to assemble an initial
	selection of images to work with. Then
	it runs image_rountine() on this selection.
	"""
	import account
	global site
	site = mwclient.Site('zh.wikipedia.org')
	#site = mwclient.Site('test2.wikipedia.org')
	site.login(account.username, account.password) 

	# # #work_with = get_images()	
	zam = mwclient.listing.Category(site, "Category:需要缩小大小的非自由文件")
	glob = zam.members()
	flub = []
	for image in glob:
		zip = image.page_title
		# print zip.encode('ascii', 'ignore')
		print(zip)
		flub.append(zip)
	print "======================================="
	image_routine(flub)
	# image_routine(flub[0:3])

	# image_list = [u'Female prime minister.jpg']
	# image_routine(image_list)


	print "We're DONE!"
	
if __name__ == '__main__':
   main()
